<?php
/**
   Copyright 2018-2019 MarioCMFlys

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**/


require("config.php");

$mysqli = new mysqli($config["db_server"], $config["db_username"], $config["db_password"], $config["db_name"]);

$query = $mysqli->query("SELECT * FROM `cameras` WHERE `Enabled`=1;");

$cams = array();

header("Content-Type: application/json");
if($query != false){
  while($row = $query->fetch_assoc()) {
    // Add cameras to a list
    $cams[] = '{"id":'.json_encode($row["ID"]).',"name":'.json_encode($row["Name"]).',"cc":'.json_encode($row["CountryCode"]).',"location":'.json_encode($row["Location"]).',"type":'.json_encode($row["Type"]).',"url":'.json_encode($row["URL"]).'}';
  }
}

// Print JSON formatted list
echo("[" . implode(",", $cams) . "]");

$mysqli->close();

?>
