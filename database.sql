SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


-- Table structure for table `cameras`
CREATE TABLE `cameras` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `CountryCode` tinytext,
  `Location` text,
  `URL` text NOT NULL,
  `Type` int(11) NOT NULL DEFAULT '1',
  `Enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `cameras`
  ADD PRIMARY KEY (`ID`);
ALTER TABLE `cameras`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

-- Table structure for table `requests`
CREATE TABLE `requests` (
  `ID` int(11) NOT NULL,
  `Camera` int(11) DEFAULT NULL,
  `Name` text,
  `CountryCode` text,
  `Location` text,
  `URL` text,
  `Type` int(11) DEFAULT NULL,
  `Enabled` tinyint(1) DEFAULT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT '1',
  `User` text NOT NULL,
  `UserComment` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `requests`
  ADD PRIMARY KEY (`ID`);
ALTER TABLE `requests`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
