/**
   Copyright 2018-2019 MarioCMFlys

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**/

var current = 0; // index of current camera object
var currentCam = null; // current camera object
var cameras = []; // array of all camera objects
var menu = null; // jquery object of the fullscreen menu
var urlParams = null; // URLSearchParams object
var stream = null; // jquery object of the stream div
var embed = false; // if an embed

// Loads a camera object
function load(cam, history = true){
  cc = cam.cc;

  if(history){
    state = {};
    state.cam = cam;
    state.index = cameras.indexOf(cam);
    window.history.pushState(state, "CamDB camera "+cam.id, window.location.pathname + "?c="+cam.id);
  }

  if(cam.cc == null) cc = "un";
  locationClick = "";
  if(cam.location != null) locationClick = ' style="cursor:pointer;" onclick="window.open(\'https://maps.google.com/?q='+encodeURIComponent(cam.location)+'\');"';
  flagTitle = cc;
  if(flagTitle == "un") flagTitle = "Unknown";
  if(cam.location != null) flagTitle = flagTitle+" ("+cam.location+")";
  $('#id').html('<span title="'+flagTitle+'" class="flag-icon flag-icon-'+cc+'"'+locationClick+'></span>&nbsp;' + ((cam.id+"").padStart(3,0)));
  $('#name').text(cam.name+'');
  type = Number(cam.type);
  if(type == 1){
    // mjpeg
    stream.html('<img src="'+cam.url+'" class="stream" onerror="error(\'An error occured while loading the stream.\')" />');
  }
  else if(type == 2){
    // play in video element
  }
  else{
    error('The web player does not support this type of stream.');
  }
  currentCam = cam;
  current = cameras.indexOf(cam);
}
// Display error message e
function error(e){
  stream.html('<p class="error"><span class="fas fa-exclamation-triangle"></span>&nbsp;'+e+'</p>');
}

// Attempts to load camera with id
function goto(id, history = true){
  for(j=0;j<cameras.length;j++){
    i = cameras[j];
    if(i.id == id){
      load(i, history);
      break;
    }
  }
}

// Attempts to load the camera before the current one in the array
function prev(){
  if(current != 0){
    load(cameras[current-1]);
  }
}

// Attempts to load the camera after the current one in the array
function next(){
  if(cameras.length != current+1){
    load(cameras[current+1]);
  }
}

// Reloads the current camera object
function reload(){
  load(currentCam);
}

// Opens popup with permalink
function create_permalink(){
  alert(window.location.protocol+"//"+window.location.host+window.location.pathname+"?c="+currentCam.id);
}

// Move the UI
function moveUI(){
  btn = $("#btnMoveUI");
  if($(".actions").hasClass("top")){
    $(".info").addClass("bottom").removeClass("top");
    $(".actions").addClass("bottom").removeClass("top");
    btn.html('<i class="fas fa-arrow-up"></i>');
    $(".actions p").insertAfter($(".actions ul"));
  }
  else{
    $(".info").addClass("top").removeClass("bottom");
    $(".actions").addClass("top").removeClass("bottom");
    btn.html('<i class="fas fa-arrow-down"></i>');
    $(".actions ul").insertAfter($(".actions p"));
  }
}

// Popup version of the goto function
function popupGoto(){
  i = prompt("Enter camera number");
  if(i){
    goto(Number(i));
  }
}

// Popup with stream URL
function popupStream(){
  alert(currentCam.url);
}

// Toggles the options menu
function toggleMenu(){
  $('#popup-menu').toggle();
}

function listCameras(){
  content = '<div class="index"><h1>camDB index</h1><input id="cameraListFilter" autofocus placeholder="Filter" onkeyup="filterCamerasList()"></input><ul class="camera-list">';
  for(i=0;i<cameras.length;i++){
    cam = cameras[i];
    cc = " ("+cam.cc+")";
    if(cam.cc == null){
      cc = "";
    }
    content = content+ '<li onclick="goto('+cam.id+');menu.toggle();">'+((cam.id+"").padStart(3,0))+" "+cam.name+cc+'</li>';
  }
  content = content + '</ul></div>';
  menu.html(content);
  menu.toggle();
}

function filterCamerasList(){
  filter = $("#cameraListFilter");  
  val = filter.val().toLowerCase();
  $(".camera-list li").filter(function() {
    $(this).toggle($(this).text().toLowerCase().indexOf(val) > -1)
  });
}

function edit(){
  window.location.href = "../edit";
}

function about(){
  menu.show();
  menu.html('<h1>camDB</h1><p><strong>&copy; MarioCMFlys 2018-2019</strong><br>Licensed to you under the <a href="https://www.apache.org/licenses/LICENSE-2.0.txt" target="_blank">Apache 2.0</a> license, see the code on <a href="https://www.gitlab.com/MarioCMFlys/camdb/" target="_blank">Gitlab</a><br>Keyboard shortcuts:<ul><li>Arrow keys, <, > - nagivate</li><li>R - Reload</li><li>L - List cameras</li><li>G - goto camera</li><li>U - show stream URL</li></ul><button onclick="menu.hide()">Close</button></p>');
}

$(document).ready(function(){
  // setup undefined variables
  menu = $('.menu');
  stream = $("#stream");
  main = $('#main');
  urlParams = new URLSearchParams(window.location.search);
  if(urlParams.has("embed") && urlParams.get("embed") == "1"){
    embed = true;
    $("div.actions").remove();
  }
  // show loading message in fullscreen menu
  menu.show();
  menu.html("<h1>Loading database...</h1>");

  // load the database using the API
  $.getJSON("../json.php", function(data){
    // populates the cameras array
    cameras = data;

    // load the first camera or the permalinked camera if there is one
    if(!urlParams.has("c")){
      load(cameras[0]);
    }
    else{
      goto(Number(urlParams.get("c")), false);
    }
    menu.hide();
    
  });

  // Key press handling
  if(!embed){
    $(document).on('keydown', function(e){
      if(menu.is(":hidden")){
        switch(e.which){
          case 37: // Left arrow
          case 188: // "<" key
            prev();
            break;
          case 39: // Right arrow
          case 190:
            next();
            break;
          case 82: // "R" key
            reload();
            break;
          case 76: // "L" key
            listCameras();
            break;
          case 71: // "G" key
            popupGoto();
            break;
          case 85: // "U" key
            popupStream();
            break;
        }
      }
    });
  }
  
  window.onpopstate = function(event){
    load(event.state.cam, false);
    current = event.state.index;
  }
});
