<?php 
/**
   Copyright 2018-2019 MarioCMFlys

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**/

require("../config.php");

if($config["user_suggestions"] != true){
  http_response_code(400);
  die("User suggestions were disabled by the administrator");
}

?>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Suggest an edit to camDB</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/lipis/flag-icon-css/17b720c7/css/flag-icon.min.css">
<style>
.button-card{
  
}
.button-card .btn{
  background-color: #FFF;
  border-color: rgba(0,0,0,0.125);
}
.button-card .btn:hover{
  background-color: #FFFFFFEE;
}
.button-card.bg-success .btn{
  color: #28A745;
}
.button-card.bg-primary .btn{
  color: #007BFF;
}
.button-card.bg-danger .btn{
  color: #DC3545;
}

.center{
  text-align: center;
}

#camEmbed iframe{
  margin-top: 15px;
  width: 720px;
  height: 480px;
  border: 0;
}

.menu-button{
  margin-left: 10px;
}
</style>
</head>
<body>
<div class="container" style="margin-top: 15px; margin-bottom: 15px;">
<div class="page" id="menu">
<h1>Suggest an edit to camDB</h1>
<p>Thank you for your interest in improving camDB. Choose an option below to view a relevant form. Please note that anything submitted here is a <i>suggestion</i>, which will be reviewed by the administrators, who determine if your change will be accepted. Your IP address will be written alongside your suggestion and will be used to prevent abuse.</p>
<hr>
<div class="row">
<div class="col-sm-4">
<div class="card center button-card text-white bg-success">
<div class="card-body">
<h4 class="card-title">Add new camera</h4>
<p class="card-text">Tell us about a camera we should add</p>
<button type="button" class="btn btn-default btn-block" id="btnCreate">Add camera...</button>
</div>
</div>
</div>
<div class="col-sm-4">
<div class="card center button-card text-white bg-primary">
<div class="card-body">
<h4 class="card-title">Edit existing camera</h4>
<p class="card-text">Change inaccurate metadata of a camera</p>
<button type="button" class="btn btn-default btn-block" id="btnEdit">Edit camera...</button>
</div>
</div>
</div>
<div class="col-sm-4">
<div class="card center button-card text-white bg-danger">
<div class="card-body">
<h4 class="card-title">Report camera</h4>
<p class="card-text">Report a camera as dead or unethical</p>
<button type="button" class="btn btn-default btn-block" id="btnReport">Report camera...</button>
</div>
</div>
</div>
</div>
</div>

<div class="page" id="create" style="display:none;">
<h3>Add camera</h3>
<hr>
<p>The camera you are adding must meet the following requirements:<br>
<input type="checkbox" class="rule-check"> The camera works<br>
<input type="checkbox" class="rule-check"> The camera is insecure<br>
<input type="checkbox" class="rule-check"> The camera is ethical<br>
<input type="checkbox" class="rule-check"> The camera is safe-for-work<br>
<input type="checkbox" class="rule-check"> The camera is not invasive of privacy<br>
</p>
<button class="btn btn-primary" id="ruleBtn">Continue</button>&nbsp;<a class="menu-button" href="#">Cancel</a>
</div>
<div class="page" id="createForm" style="display:none;">
<h3>Add camera</h3>
<hr>
<form action="create.php" method="POST">
<div class="form-group">
<label>Name</label>
<input type="input" class="form-control" name="name" placeholder="" required>
</div>
<div class="form-group">
<label>Country code</label>
<div class="input-group flag-input">
<input type="input" class="form-control" name="cc" placeholder="">
<div class="input-group-append"><span class="input-group-text"><span class=""></span></div>
</div>
</div>
<div class="form-group">
<label>Location</label>
<input type="input" class="form-control" name="location" placeholder="">
</div>
<div class="form-group">
<label>URL</label>
<input type="input" class="form-control" name="url" placeholder="" required>
</div>
<div class="form-group">
<label>Type</label>
<select id="type" class="form-control" name="type" required>
<option value="1">Mjpeg / Axis compatible</option>
<option value="2">Video</option>
<option value="3">Other</option>
</select>
</div>
<hr>
<div class="form-group">
<label>Comment</label>
<textarea class="form-control" name="comment"></textarea>
</div>
<button class="btn btn-primary">Submit</button>&nbsp;<a class="menu-button" href="#">Cancel</a>
</form>
</div>

<div class="page" id="edit" style="display:none;">
<h3>Edit Camera</h3>
<p>Leave blank for no change</p>
<hr>
<form action="edit.php" method="POST">
<div class="form-group">
<label>Camera #</label>
<input type="input" class="form-control camera-number" name="camera" placeholder="" required>
</div>
<div class="form-group">
<label>Name</label>
<input type="input" class="form-control" name="name" placeholder="">
</div>
<div class="form-group">
<label>Country code</label>
<div class="input-group flag-input">
<input type="input" class="form-control" name="cc" placeholder="">
<div class="input-group-append"><span class="input-group-text"><span class=""></span></div>
</div>
</div>
<div class="form-group">
<label>Location</label>
<input type="input" class="form-control" name="location" placeholder="">
</div>
<div class="form-group">
<label>URL</label>
<input type="input" class="form-control" name="url" placeholder="">
</div>
<div class="form-group">
<label>Type</label>
<select id="type" class="form-control" name="type">
<option value="NC">(NO CHANGE)</option>
<option value="1">Mjpeg / Axis compatible</option>
<option value="2">Video</option>
<option value="3">Other</option>
</select>
</div>
<hr>
<div class="form-group">
<label>Comment</label>
<textarea class="form-control" name="comment"></textarea>
</div>
<button class="btn btn-primary">Submit</button>&nbsp;<a class="menu-button" href="#">Cancel</a>
</form>
</div>

<div class="page" id="report" style="display:none;">
<h3>Report Camera</h3>
<hr>
<form action="report.php" method="POST">
<input type="hidden" name="enabled" value="0">

<div class="form-group">
<label>Camera #</label>
<input type="input" class="form-control camera-number" name="camera" placeholder="" required>
</div>
<div class="form-group">
<label>Describe why you are reporting this camera</label>
<textarea class="form-control" name="comment" required></textarea>
</div>
<button class="btn btn-primary">Submit</button>&nbsp;<a class="menu-button" href="#">Cancel</a>
</form>
</div>

<div class="page" id="selectCam" style="display: none;">

<div class="form-group">
<label>Camera #</label>
<input id="camNum" type="text" class="form-control"></input>
</div>
<button class="btn btn-secondary" id="camPreview">Preview</button>&nbsp;<button id="camContinue" class="btn btn-primary">Continue</button>&nbsp;<a class="menu-button" href="#">Cancel</a>
<div id="camEmbed"></div>
</div>

</div>
<script>
var action = "";

function page(name){
  $(".page").hide();
  $(".page#"+name).show();
}

function ruleCheck(){
  if($('.rule-check:checked').length == $('.rule-check').length){
    $('#ruleBtn').prop("disabled", false);
  }
  else{
    $('#ruleBtn').prop("disabled",true);
  }
}

$(document).ready(function(){
  $("#btnCreate").on("click", function(){page("create");});
  $("#btnEdit").on("click", function(){action = "edit"; page("selectCam");});
  $("#btnReport").on("click", function(){action = "report"; page("selectCam");});

  $("#camPreview").on("click", function(){
    $("#camEmbed").html('<iframe src="../web/?c='+parseInt($("#camNum").val())+'&embed=1">Error: this browser does not support iframes</iframe>');
  });
  
  $("#camContinue").on("click", function(){
    $(".camera-number").val($("#camNum").val());
    page(action);
  });
  $(".menu-button").on("click", function(){page("menu");});
  $(".rule-check").on("change", function(){
    ruleCheck();
  });
  $("#ruleBtn").on("click", function(){
    if($("#ruleBtn:disabled").length == 0){
      page("createForm");
    }
  });
  ruleCheck();

  $("div.flag-input input").on("input", function(event){
    inp = event.target;
    flag = inp.value;
    if(flag == "") flag = "un";
    $("div.flag-input span > span").removeClass().attr('class', 'flag-icon flag-icon-'+flag);
  });
});
</script>
</body>
</html>
