<?php 
/**
   Copyright 2018-2019 MarioCMFlys

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**/

require("../config.php");

if($config["user_suggestions"] != true){
  http_response_code(400);
  die("User suggestions were disabled by the administrator");
}
if((!isset($_POST["camera"])) |
   (!isset($_POST["name"])) |
   (!isset($_POST["cc"])) |
   (!isset($_POST["location"])) |
   (!isset($_POST["url"])) |
   (!isset($_POST["type"])) |
   (!isset($_POST["comment"]))){
  http_response_code(400);
  die("Fields are missing");
}

// inputs:
// camera, name, cc, location, url, type, comment (and optionally "noredirect")

$mysqli = new mysqli($config["db_server"], $config["db_username"], $config["db_password"], $config["db_name"]);

$query = $mysqli->prepare("INSERT INTO `requests`(`ID`, `Camera`, `Name`, `CountryCode`, `Location`, `URL`, `Type`, `Enabled`, `Active`, `User`, `UserComment`) VALUES (null,?,?,?,?,?,?,?,?,?,?);");
$query->bind_param("issssiiiss", $cam, $name, $cc, $loc, $url, $type, $enabled, $active, $user, $comment);

$cam = ((int) $_POST["camera"]);
$name = $_POST["name"];
if($name == "") $name = null;
$cc = $_POST["cc"];
if($cc == "") $cc = null;
$loc = $_POST["location"];
if($loc == "") $loc = null;
$url = $_POST["url"];
if($url == "") $url = null;
($_POST["type"] == "NC" ? $type = null : $type = ((int) $_POST["type"]));
$enabled = 1;
$active = 1;
$user = "guest_" . $_SERVER["REMOTE_ADDR"] . "_" . $_SERVER["HTTP_X_FORWARDED_FOR"];
$comment = $_POST["comment"];
if($comment == "") $comment = null;

$query->execute();
$query->close();
$mysqli->close();

// Success!

if(isset($_POST["noredirect"])){
  http_response_code(204);
  die();
}
header("Location: success.php");
die("Request successful");
?>
