<?php 
/**
   Copyright 2018-2019 MarioCMFlys

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**/

require("auth.php");

$mysqli = new mysqli($config["db_server"], $config["db_username"], $config["db_password"], $config["db_name"]);

if(isset($_POST["rid"]) || isset($_GET["xid"])){
  $rid = $_GET["xid"];
  if(isset($_POST["rid"])) $rid = $_POST["rid"];
  $stmt = $mysqli->prepare("UPDATE `requests` SET `Active`=0 WHERE `ID`=?;");
  $stmt->bind_param("i", $rid);
  $stmt->execute();
  $stmt->close();
  if(isset($_GET["xid"])){
    header("Location: requests.php");
    die("Success! Redirecting...");
  }
}

if(isset($_POST["submit"])){
  $sub = $_POST["submit"];
  if($sub == "NEW"){
    $stmt = $mysqli->prepare("INSERT INTO `cameras`(`ID`, `Name`, `CountryCode`, `Location`, `URL`, `Type`, `Enabled`) VALUES (null,?,?,?,?,?,?);");
    $stmt->bind_param("ssssii", $name, $cc, $location, $url, $type, $enabled);
    $name = $_POST["name"];
    $cc = $_POST["cc"];
    if($cc == "") $cc = null;
    $location = $_POST["location"];
    if($location == "") $location = null;
    $url = $_POST["url"];
    $type = $_POST["type"];
    $enabled = $_POST["enabled"];
    $id = $_POST["id"];

    $stmt->execute();
    $stmt->close();
    $mysqli->close();
    header("Location: requests.php");
    die("Success! Redirecting...");
  }
  else{
    $id = $sub;
    // existing camera
    $stmt = $mysqli->prepare("SELECT `Name`,`CountryCode`,`Location`,`URL`,`Type`,`Enabled` FROM `cameras` WHERE `ID`=?;");
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $row = $stmt->get_result()->fetch_assoc();
    $stmt->close();
    $stmt = $mysqli->prepare("UPDATE `cameras` SET `Name`=?,`CountryCode`=?,`Location`=?,`URL`=?,`Type`=?,`Enabled`=? WHERE `ID`=?;");
    $stmt->bind_param("ssssiii", $name, $cc, $location, $url, $type, $enabled, $id);
    $name = $_POST["name"];
    if($name == "") $name = $row["Name"];
    $cc = $_POST["cc"];
    if($cc == "") $cc = $row["CountryCode"];
    $location = $_POST["location"];
    if($location == "") $location = $row["Location"];
    $url = $_POST["url"];
    if($url == "") $url = $row["URL"];
    $type = $_POST["type"];
    if($type == "NC") $type = $row["Type"];
    $enabled = $_POST["enabled"];
    if($enabled == "NC") $enabled = $row["Enabled"];

    $stmt->execute();
    $stmt->close();
    $mysqli->close();
    header("Location: requests.php");
    die("Success! Redirecting...");
  }
}



if(!isset($_GET["id"])){
  die("Error: no ID");
}
else{
  $id = $_GET["id"];
  
  $stmt = $mysqli->prepare("SELECT `Camera`,`Name`,`CountryCode`,`Location`,`URL`,`Type`,`Enabled`,`Active`,`User`,`UserComment` FROM `requests` WHERE id=?;");
  $stmt->bind_param("i", $id);
  $stmt->execute();
  $row = $stmt->get_result()->fetch_assoc();
  
  $stmt->close();
  $mysqli->close();
  
  $camera = $row["Camera"];
  $name = $row["Name"];
  $cc = $row["CountryCode"];
  if($cc === null) $cc = "";
  $location = $row["Location"];
  if($location === null) $location = "";
  $url = $row["URL"];
  $type = $row["Type"];
  $enabled = $row["Enabled"];
  if($enabled == "") $enabled = "0";
  $active = $row["Active"];
  $user = $row["User"];
  $comment = $row["UserComment"];
}
?>
<html>
<head>
<?php include("header.html"); ?>
</head>
<body>
<div class="container">
<form action="view-request.php" method="POST">
<input type="hidden" name="submit" value="<?php echo($camera == null ? "NEW" : $camera); ?>">
<input type="hidden" name="rid" value="<?php echo $id; ?>">
<h3 class="header">Reviewing request #<?php echo $id; ?>
<div class="float-right">
<a class="btn btn-default" href="requests.php">Exit Request Reviewer</a>
<a class="btn btn-danger" href="view-request.php?xid=<?php echo $id; ?>">Reject</a>
<button type="submit" class="btn btn-success">Accept</button>
</div>
</h3>
<?php if($active == "0"){?><div class="alert alert-warning"><strong>Notice:</strong> This edit request has been closed by you or another administrator.</div><?php } ?>
<div class="form-group">
<label>Name</label>
<div class="input-group">
<div class="input-group-prepend"><span class="input-group-text"><?php echo (($camera == null ? "NEW" : $camera)); ?></span></div>
<input id="inputname" class="form-control" name="name" value="<?php echo htmlspecialchars($name); ?>">
</div>
</div>
<div class="form-group">
<label>Country code (<a href="https://www.countrycode.org/" target="_blank">list</a>)</label>
<div class="input-group">
<input id="flaginput" class="form-control" name="cc" value="<?php echo htmlspecialchars($cc); ?>">
<div class="input-group-append"><span class="input-group-text"><span id="flagicon" class=""></span></span></div>
</div>
</div>
<div class="form-group">
<label>Location (format: 40.7129149,-74.0066528)</label>
<input class="form-control" name="location" value="<?php echo htmlspecialchars($location); ?>">
</div>
<div class="form-group">
<label>URL</label>
<input class="form-control" name="url" value="<?php echo htmlspecialchars($url); ?>">
</div>
<div class="form-group">
<label>Type</label>
<select id="type" class="form-control" name="type" required>
<option value="NC">(NO CHANGE)</option>
<option value="1">Mjpeg / Axis compatible</option>
<option value="2">Video</option>
<option value="3">Other</option>
</select>
</div>
<div class="form-group">
<label>Status</label>
<select id="enabled" class="form-control" name="enabled" required>
<option value="NC">(NO CHANGE)</option>
<option value="1">Include this camera in the directory and web viewer</option>
<option value="0">Omit this camera from the directory and web viewer</option>
</select>
</div>
<hr>

<div class="form-group">
<label>User</label>
<input class="form-control" value="<?php echo htmlspecialchars($user); ?>" disabled>
</div>
<div class="form-group">
<label>User Comment</label>
<textarea class="form-control" disabled><?php echo htmlspecialchars($comment); ?></textarea>
</div>

</form>
</div>
<script>
$(document).ready(function(){
  $("select#type").val("<?php echo ($type == "" ? 'NC' : htmlspecialchars($type)); ?>");
  $("select#enabled").val("<?php echo ($enabled == "" ? 'NC' : htmlspecialchars($enabled)); ?>");
  $("input#flaginput").on('blur', function(){
    inp = $("input#flaginput");
    flag = inp.val();
    if(inp.val() == "") flag = "un";
    $("span#flagicon").removeClass().attr('class', 'flag-icon flag-icon-'+flag);
  });
  $("input#flaginput").focus();
  $("input#inputname").focus();
});
</script>
</body>
</html>
