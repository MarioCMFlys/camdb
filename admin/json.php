<?php
/**
   Copyright 2018-2019 MarioCMFlys

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**/

require("auth.php");

$mysqli = new mysqli($config["db_server"], $config["db_username"], $config["db_password"], $config["db_name"]);

if(isset($_GET["requests"])){
  $query = $mysqli->query("SELECT * FROM `requests` WHERE `Active`=1;");

  $reqs = array();

  header("Content-Type: application/json");
  if($query != false){
    while($row = $query->fetch_assoc()) {
      $e = false;
      if(((int) $row["Enabled"]) == 1){
        $e = true;
      }
      $a = false;
      if(((int) $row["Active"]) == 1){
        $a = true;
      }
      // Add cameras to a list
      $reqs[] = '{"id":'.json_encode($row["ID"]).',"camera":'.json_encode($row["Camera"]).',"name":'.json_encode($row["Name"]).',"cc":'.json_encode($row["CountryCode"]).',"location":'.json_encode($row["Location"]).',"url":'.json_encode($row["URL"]).',"type":'.json_encode($row["Type"]).',"enabled":'.json_encode($e).',"active":'.json_encode($a).',"user":'.json_encode($row["User"]).',"comment":'.json_encode($row["UserComment"]).'}';
    }
  }

  // Print JSON formatted list
  echo("[" . implode(",", $reqs) . "]");
}
else{
  $query = $mysqli->query("SELECT * FROM `cameras`;");

  $cams = array();

  header("Content-Type: application/json");
  if($query != false){
    while($row = $query->fetch_assoc()) {
      $e = false;
      if(((int) $row["Enabled"]) == 1){
        $e = true;
      }
      // Add cameras to a list
      $cams[] = '{"id":'.json_encode($row["ID"]).',"name":'.json_encode($row["Name"]).',"cc":'.json_encode($row["CountryCode"]).',"location":'.json_encode($row["Location"]).',"type":'.json_encode($row["Type"]).',"url":'.json_encode($row["URL"]).',"enabled":'.json_encode($e).'}';
    }
  }

  // Print JSON formatted list
  echo("[" . implode(",", $cams) . "]");

}

$mysqli->close();

?>
