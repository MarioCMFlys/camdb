<?php 
/**
   Copyright 2018-2019 MarioCMFlys

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**/

require("auth.php");
?>
<html>
<head>
<?php include("header.html"); ?>
</head>
<body>
<div class="container">
<h1 class="header">camDB Admin Control Center</h1>
<h3>Edit Requests
<input class="form-control" id="filter" type="input" placeholder="Filter" onkeyup="filterCamerasList()">
<a class="btn btn-primary float-right" href="#" id="view-next">Review Next Request</a>
<a class="btn btn-default float-right" href="index.php">View Live Cameras</a>
</h3>
<table class="table table-hover">
<thead><tr><th>ID</th><th>User</th><th>Actions</th></tr></thead>
<tbody id="requests-table"></tbody>
</table>
<div class="footer">
Close your browser to log out.
</div>
</div>
<script>
function filterCamerasList(){
  filter = $("#filter");  
  val = filter.val().toLowerCase();
  $("#requests-table tr").filter(function() {
    $(this).toggle($(this).text().toLowerCase().indexOf(val) > -1);
  });
}

$(document).ready(function(){

  $.getJSON("json.php?requests=1", function(data){
    for(j = 0; j < data.length; j++){
      i = data[j];
      if(j == 0) $('#view-next').attr("href", "view-request.php?id="+i.id);
      cc = "";
      if(i.cc != null) cc = i.cc;
      $('#requests-table').append('<tr><td>'+i.id+'</td><td>'+i.user+'</td><td><a href="view-request.php?id='+i.id+'">Review</a></td></tr>');
    }
  });
});
</script>
</body>
</html>
