<?php 
/**
   Copyright 2018-2019 MarioCMFlys

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**/

require("auth.php");

$mysqli = new mysqli($config["db_server"], $config["db_username"], $config["db_password"], $config["db_name"]);

if(isset($_POST["id"]) || isset($_POST["new"])){
  if(isset($_POST["new"])){
    $stmt = $mysqli->prepare("INSERT INTO `cameras`(`ID`, `Name`, `CountryCode`, `Location`, `URL`, `Type`, `Enabled`) VALUES (null,?,?,?,?,?,?);");
    $stmt->bind_param("ssssii", $name, $cc, $location, $url, $type, $enabled);
  }
  else{
    $stmt = $mysqli->prepare("UPDATE `cameras` SET `Name`=?,`CountryCode`=?,`Location`=?,`URL`=?,`Type`=?,`Enabled`=? WHERE `ID`=?;");
    $stmt->bind_param("ssssiii", $name, $cc, $location, $url, $type, $enabled, $id);
  }
  $name = $_POST["name"];
  $cc = $_POST["cc"];
  if($cc == "") $cc = null;
  $location = $_POST["location"];
  if($location == "") $location = null;
  $url = $_POST["url"];
  $type = $_POST["type"];
  $enabled = $_POST["enabled"];
  $id = $_POST["id"];

  $stmt->execute();
  $stmt->close();
  $mysqli->close();
  header("Location: index.php");
  die("Success! Redirecting...");

}

if((!isset($_GET["id"])) && (!isset($_GET["new"]))){
  $mysqli->close();
  die("Error: no ID");
}

if(isset($_GET["new"])){
  $new = true;
  $name = "";
  $cc = "";
  $location = "";
  $url = "";
  $type = 1;
}
else{
  $new = false;
  $id = $_GET["id"];
  
  
  $stmt = $mysqli->prepare("SELECT `Name`,`CountryCode`,`Location`,`URL`,`Type`,`Enabled` FROM `cameras` WHERE id=?;");
  $stmt->bind_param("i", $id);
  $stmt->execute();
  $row = $stmt->get_result()->fetch_assoc();
  
  $stmt->close();
  $mysqli->close();
  
  
  $name = $row["Name"];
  $cc = $row["CountryCode"];
  if($cc === null) $location = "";
  $location = $row["Location"];
  if($location === null) $location = "";
  $url = $row["URL"];
  $type = $row["Type"];
  $enabled = $row["Enabled"];
}
?>
<html>
<head>
<?php include("header.html"); ?>
</head>
<body>
<div class="container">
<form action="manage-camera.php" method="POST">
<h3 class="header"><?php if($new){ ?>Creating new camera<?php }else{ ?>Editing camera <?php echo $id; } ?>
<div class="float-right">
<button type="submit" class="btn btn-success">Save</button>
<a class="btn btn-danger" href="index.php">Cancel</a>
</div>
</h3>
<?php if($new){?><input name="new" type="hidden" value="0"><?php }else{ ?><input name="id" type="hidden" value="<?php echo $id; ?>"><?php } ?>
<div class="form-group">
<label>Name</label>
<input id="inputname" class="form-control" name="name" value="<?php echo $name; ?>" required>
</div>
<div class="form-group">
<label>Country code (<a href="https://www.countrycode.org/" target="_blank">list</a>)</label>
<div class="input-group">
<input id="flaginput" class="form-control" name="cc" value="<?php echo $cc; ?>">
<div class="input-group-append"><span class="input-group-text"><span id="flagicon" class=""></span></div>
</div>
</div>
<div class="form-group">
<label>Location (format: 40.7129149,-74.0066528)</label>
<input class="form-control" name="location" value="<?php echo $location; ?>">
</div>
<div class="form-group">
<label>URL</label>
<input class="form-control" name="url" value="<?php echo $url; ?>" required>
</div>
<div class="form-group">
<label>Type</label>
<select id="type" class="form-control" name="type" required>
<option value="1">Mjpeg / Axis compatible</option>
<option value="2">Video</option>
<option value="3">Other</option>
</select>
</div>
<div class="form-group">
<label>Status</label>
<select id="enabled" class="form-control" name="enabled" required>
<option value="1">Include this camera in the directory and web viewer</option>
<option value="0">Omit this camera from the directory and web viewer</option>
</select>
</div>
</form>
</div>
<script>
$(document).ready(function(){
  $("select#type").val("<?php echo $type; ?>");
  $("select#enabled").val("<?php echo $enabled; ?>");
  $("input#flaginput").on('blur', function(){
    inp = $("input#flaginput");
    flag = inp.val();
    if(inp.val() == "") flag = "un";
    $("span#flagicon").removeClass().attr('class', 'flag-icon flag-icon-'+flag);
  });
  $("input#flaginput").focus();
  $("input#inputname").focus();
});
</script>
</body>
</html>
